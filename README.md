# Student Fee Reversal RESTful Web Service

## Description

projects-feeReersalService has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made.  After conversion was done, PHPUnit has been executed.

## Special consideration for PHP/ColdFusion projects
As part of the move to Git, we have restructured the projects on the shared development servers.  All clones of git projects will be created in /opt/webapps/wc and a symlink will be created to the appropriate web directory of your project in /opt/webapps/phpapps or /opt/webapps/cfapps.  If you need to make your own personal copy, you can do that following something similar to these instructions:

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/finance

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.