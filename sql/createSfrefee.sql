BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SFREFEE';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
--------------------------------------------------------
--  DDL for Table SFREFEE
--------------------------------------------------------

  CREATE TABLE SFREFEE 
   (    SFREFEE_TERM_CODE VARCHAR2(6 CHAR), 
    SFREFEE_PIDM NUMBER(8,0), 
    SFREFEE_DETL_CODE VARCHAR2(4 CHAR), 
    SFREFEE_ACTIVITY_DATE DATE, 
    SFREFEE_SURROGATE_ID NUMBER(19,0), 
    SFREFEE_VERSION NUMBER(19,0), 
    SFREFEE_USER_ID VARCHAR2(30 CHAR), 
    SFREFEE_DATA_ORIGIN VARCHAR2(30 CHAR), 
    SFREFEE_VPDI_CODE VARCHAR2(6 CHAR)
   ) ;

   COMMENT ON COLUMN SFREFEE.SFREFEE_TERM_CODE IS 'This field identifies the term associated with the additional registration        fee(s).           ';
   COMMENT ON COLUMN SFREFEE.SFREFEE_PIDM IS 'Internal Identification Number of the registrant for which the additional         registration fee(s) apply.           ';
   COMMENT ON COLUMN SFREFEE.SFREFEE_DETL_CODE IS 'This field identifies the additional registration fee detail code.           ';
   COMMENT ON COLUMN SFREFEE.SFREFEE_ACTIVITY_DATE IS 'This field specifies the most current date record was created or updated.           ';
   COMMENT ON COLUMN SFREFEE.SFREFEE_SURROGATE_ID IS 'SURROGATE ID: Immutable unique key';
   COMMENT ON COLUMN SFREFEE.SFREFEE_VERSION IS 'VERSION: Optimistic lock token.';
   COMMENT ON COLUMN SFREFEE.SFREFEE_USER_ID IS 'USER ID: The user ID of the person who inserted or last updated this record.';
   COMMENT ON COLUMN SFREFEE.SFREFEE_DATA_ORIGIN IS 'DATA ORIGIN: Source system that created or updated the data.';
   COMMENT ON COLUMN SFREFEE.SFREFEE_VPDI_CODE IS 'VPDI CODE: Multi-entity processing code.';
   COMMENT ON TABLE SFREFEE  IS 'Student Registration Additional Fees Repeating Table';
--------------------------------------------------------
--  DDL for Index SFREFEE_KEY_INDEX
--------------------------------------------------------

  CREATE INDEX SFREFEE_KEY_INDEX ON SFREFEE (SFREFEE_TERM_CODE, SFREFEE_PIDM, SFREFEE_DETL_CODE) 
  ;
--------------------------------------------------------
--  Constraints for Table SFREFEE
--------------------------------------------------------

  ALTER TABLE SFREFEE MODIFY (SFREFEE_TERM_CODE NOT NULL ENABLE);
  ALTER TABLE SFREFEE MODIFY (SFREFEE_PIDM NOT NULL ENABLE);
  ALTER TABLE SFREFEE MODIFY (SFREFEE_DETL_CODE NOT NULL ENABLE);
  ALTER TABLE SFREFEE MODIFY (SFREFEE_ACTIVITY_DATE NOT NULL ENABLE);
