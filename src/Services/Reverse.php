<?php

namespace MiamiOH\ProjectsFeereversalservice\Services;

class Reverse extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'STUFEEREV_DB';
    private $dbh;

    private $config = array();

    public function setDatabase($db)
    {
        $this->dbh = $db->getHandle($this->dbDataSourceName);
    }

    public function applyFeeReversal()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Student Fees', 'Reverse', 'apply');

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();

        if (!(isset($data['bannerId']) && $data['bannerId'])) {
            throw new \Exception(__CLASS__ . '::applyFeeReversal requires bannerId in data body');
        }

        if (!(isset($data['termCode']) && $data['termCode'])) {
            throw new \Exception(__CLASS__ . '::applyFeeReversal requires termCode in data body');
        }

        if (!(isset($data['feeDetailCode']) && $data['feeDetailCode'])) {
            throw new \Exception(__CLASS__ . '::applyFeeReversal requires feeDetailCode in data body');
        }

        $recordCount = $this->dbh->queryfirstcolumn('
        select count(*)
          from sfrefee
          where sfrefee_pidm = (
                                  select szbuniq_pidm 
                                    from szbuniq
                                    where szbuniq_banner_id = ?
                                )
            and sfrefee_term_code = ?
            and sfrefee_detl_code = ?
      ', $data['bannerId'], $data['termCode'], $data['feeDetailCode']);

        $this->log->debug('Fee record count for ' . $data['bannerId'] . ' is ' . $recordCount);

        if ((int)$recordCount === 0) {
            $this->dbh->perform('
          insert into sfrefee (sfrefee_pidm, sfrefee_term_code, sfrefee_detl_code, sfrefee_activity_date)
            values ((select szbuniq_pidm 
                    from szbuniq
                    where szbuniq_banner_id = ?), ?, ?, sysdate)
        ', $data['bannerId'], $data['termCode'], $data['feeDetailCode']);

            $this->log->debug('Fee code ' . $data['feeDetailCode'] . ' inserted for ' .
                $data['bannerId'] . ' term ' . $data['termCode']);
        }

        return $response;
    }

}

/*

Begin-select
count(1)  &count
from sfrefee
where sfrefee_pidm=&s_pidm
AND sfrefee_term_code=$term_code
AND sfrefee_DETL_code=$detail_code
End-select

insert into saturn.sfrefee
(
sfrefee_term_code,
sfrefee_pidm,
sfrefee_detl_code,
sfrefee_activity_date
)
values
(
$term_code,
&s_pidm,
$detail_code,
sysdate
)

*/