<?php

namespace MiamiOH\ProjectsFeereversalservice\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class FeeReversalResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StudentFeeReversal',
            'class' => 'MiamiOH\ProjectsFeereversalservice\Services\Reverse',
            'description' => 'Provide student fee reversal service.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'update',
            'name' => 'finance.accountsReceivable.student.v1.fee.reverse',
            'description' => 'Apply a fee reversal to the given student.',
            'pattern' => '/finance/accountsReceivable/student/v1/fee/reverse',
            'service' => 'StudentFeeReversal',
            'method' => 'applyFeeReversal',
            'returnType' => 'none',
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}