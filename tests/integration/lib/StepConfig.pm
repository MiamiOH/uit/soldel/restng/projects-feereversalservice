#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw($server $authInfo $dbh);

our $server = 'http://ws/api';

our $authInfo = {
    'path' => '/authentication/v1',
    'username' => 'STUINS_WS_USER',
    'password' => 'insurance',
};

our $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

1;