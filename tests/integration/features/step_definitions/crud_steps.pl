#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

Given qr/an empty (\S+) table/, sub { 
    $dbh->do(qq{ delete from $1 });
};

# a book with the title "My Book"
Given qr/a (\S+) record with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{'object'}{$2} = $3;
};

# that the book has been added
Given qr/that the (\S+) record has been added/, sub {
    ok($1 eq S->{'objectType'}, "Create a new $1");

    if ($1 eq S->{'objectType'}) {
        my $fields = join(', ', keys %{S->{'object'}});
        my $values = join(', ', map { "'" . $_ . "'"} values %{S->{'object'}});
        my $query = "insert into " . S->{'objectType'} . '(' . $fields . ') values (' . $values . ')';

        $dbh->do($query);
    }
};

Given qr/that a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};

When qr/I have a (\S+) with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{'object'}{$2} = $3;
};

Then qr/a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};

