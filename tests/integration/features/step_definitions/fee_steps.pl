#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {
    $dbh->do(qq{ delete from szbuniq });
    $dbh->do(qq{ delete from sfrefee });

    $dbh->do(q{
        insert into szbuniq (szbuniq_banner_id, szbuniq_unique_id, szbuniq_pidm)
            values (?, ?, ?)
        }, undef, '+11118888', 'doej', '11118888');

    $dbh->do(q{
        insert into szbuniq (szbuniq_banner_id, szbuniq_unique_id, szbuniq_pidm)
            values (?, ?, ?)
        }, undef, '+11118889', 'smithj', '11118889');

    $dbh->do(q{
        insert into sfrefee (sfrefee_pidm, sfrefee_term_code, sfrefee_detl_code, sfrefee_activity_date)
            values (?, ?, ?, sysdate)
        }, undef, '11118889', '201610', '3956');

};

Given qr/(\d+) fee records are found for ([^ ]+) in term ([^ ]+) for detail code ([^ ]+)/, sub {
    my $expectedCount = $1;
    my $bannerId = $2;
    my $termCode = $3;
    my $feeDetailCode = $4;

    my($count) = $dbh->selectrow_array(q{
        select count(*)
          from sfrefee
          where sfrefee_pidm = (
                                  select szbuniq_pidm 
                                    from szbuniq
                                    where szbuniq_banner_id = ?
                                )
            and sfrefee_term_code = ?
            and sfrefee_detl_code = ?
        }, undef, $bannerId, $termCode, $feeDetailCode);

    ok($count == $expectedCount, "Expected count $expectedCount for $bannerId ($termCode, $feeDetailCode) does not " .
        "matches real count $count");
};

Then qr/(\d+) fee records are found for ([^ ]+) in term ([^ ]+) for detail code ([^ ]+)/, sub {
    my $expectedCount = $1;
    my $bannerId = $2;
    my $termCode = $3;
    my $feeDetailCode = $4;

    my($count) = $dbh->selectrow_array(q{
        select count(*)
          from sfrefee
          where sfrefee_pidm = (
                                  select szbuniq_pidm 
                                    from szbuniq
                                    where szbuniq_banner_id = ?
                                )
            and sfrefee_term_code = ?
            and sfrefee_detl_code = ?
        }, undef, $bannerId, $termCode, $feeDetailCode);

    ok($count == $expectedCount, "Expected count $expectedCount for $bannerId ($termCode, $feeDetailCode) does not " .
        "matches real count $count");
};

