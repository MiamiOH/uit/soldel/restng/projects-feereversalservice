# Test usage of the fee reversal service
Feature: Apply a fee reversal to a given student for a given fee code
  As a consumer of the fee reversal service
  I want to request a fee reversal given a student, term code and fee code
  In order to have the given fee removed from their records

  Background:
    Given the test data is ready

  Scenario: Require authentication to access the resource
    Given a REST client
    When I make a PUT request for /finance/accountsReceivable/student/v1/fee/reverse
    Then the HTTP status code is 401

  Scenario: Post a fee waiver for a user
    Given a REST client
    And a valid token for an authorized user
    And 0 fee records are found for +11118888 in term 201610 for detail code 3956
    When I have a feeRequest with the bannerId "+11118888"
    And I have a feeRequest with the termCode "201610"
    And I have a feeRequest with the feeDetailCode "3956"
    And I give the HTTP Content-type header with the value "application/json"
    And I make a PUT request for /finance/accountsReceivable/student/v1/fee/reverse
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And 1 fee records are found for +11118888 in term 201610 for detail code 3956

  Scenario: Posting a fee waiver when there is an existing record does not create a second
    Given a REST client
    And a valid token for an authorized user
    And 1 fee records are found for +11118889 in term 201610 for detail code 3956
    When I have a feeRequest with the bannerId "+11118889"
    And I have a feeRequest with the termCode "201610"
    And I have a feeRequest with the feeDetailCode "3956"
    And I give the HTTP Content-type header with the value "application/json"
    And I make a PUT request for /finance/accountsReceivable/student/v1/fee/reverse
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And 1 fee records are found for +11118889 in term 201610 for detail code 3956
