<?php

namespace MiamiOH\ProjectsFeereversalservice\Tests\Unit;

use MiamiOH\RESTng\App;

class FeeReverseTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $subject;

    private $querySql = '';
    private $queryBannerId = '';
    private $queryTermCode = '';
    private $queryFeeDetailCode = '';

    private $testRecords = array(
        '+11112345' => array(
            'recordCount' => 0
        ),
        '+11119876' => array(
            'recordCount' => 1
        ),
    );

    protected function setUp()
    {

        $this->querySql = '';
        $this->queryBannerId = '';
        $this->queryTermCode = '';
        $this->queryFeeDetailCode = '';

        $apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        // always authorized for these tests.
        $apiUser->method('isAuthorized')->willReturn(1);

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'perform'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->subject = new \MiamiOH\ProjectsFeereversalservice\Services\Reverse();
        $this->subject->setDatabase($db);
        $this->subject->setApiUser($apiUser);

    }

    public function testApplyFeeReversal()
    {

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithBannerId')),
                $this->callback(array($this, 'queryfirstcolumnWithTermCode')),
                $this->callback(array($this, 'queryfirstcolumnWithFeeDetailCode')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithBannerId')),
                $this->callback(array($this, 'performWithTermCode')),
                $this->callback(array($this, 'performWithFeeDetailCode')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $this->queryBannerId = '+11112345';
        $this->queryTermCode = '201510';
        $this->queryFeeDetailCode = '1234';

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'bannerId' => $this->queryBannerId,
                'feeDetailCode' => $this->queryFeeDetailCode,
                'termCode' => $this->queryTermCode,
            )
        );

        $this->subject->setRequest($request);

        $resp = $this->subject->applyFeeReversal();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage MiamiOH\ProjectsFeereversalservice\Services\Reverse::applyFeeReversal requires bannerId in data body
     */
    public function testApplyFeeReversalMissingBannerId()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'feeDetailCode' => '1234',
                'termCode' => '201510',
            )
        );

        $this->subject->setRequest($request);

        $resp = $this->subject->applyFeeReversal();

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage MiamiOH\ProjectsFeereversalservice\Services\Reverse::applyFeeReversal requires feeDetailCode in data body
     */
    public function testApplyFeeReversalMissingFeeDetailCode()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'bannerId' => '+11112345',
                'termCode' => '201510',
            )
        );

        $this->subject->setRequest($request);

        $resp = $this->subject->applyFeeReversal();

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage MiamiOH\ProjectsFeereversalservice\Services\Reverse::applyFeeReversal requires termCode in data body
     */
    public function testApplyFeeReversalMissingTermCode()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            array(
                'bannerId' => '+11112345',
                'feeDetailCode' => '1234',
            )
        );

        $this->subject->setRequest($request);

        $resp = $this->subject->applyFeeReversal();

    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function queryfirstcolumnWithBannerId($subject)
    {
        return $subject === $this->queryBannerId;
    }

    public function queryfirstcolumnWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function queryfirstcolumnWithFeeDetailCode($subject)
    {
        return $subject === $this->queryFeeDetailCode;
    }

    public function queryfirstcolumnMock()
    {
        return $this->testRecords[$this->queryBannerId]['recordCount'];
    }

    public function performWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function performWithBannerId($subject)
    {
        return $subject === $this->queryBannerId;
    }

    public function performWithTermCode($subject)
    {
        return $subject === $this->queryTermCode;
    }

    public function performWithFeeDetailCode($subject)
    {
        return $subject === $this->queryFeeDetailCode;
    }

    public function performMock()
    {
        return true;
    }

}